#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <math.h>
#include <algorithm>
#include <limits>
#include <ctime>

#include "task.h"

#define SIZETMAX numeric_limits<size_t>::max()


using namespace std;

int main()
{
    string pliki[11];
    for(size_t i=10;i<=20;i++)
    {
        stringstream nazwa_tmp;
        nazwa_tmp << "data" << i << ".txt";
        pliki[i-10] = nazwa_tmp.str();
    }

    for(auto & nazwa_pliku : pliki)
    {

        unsigned int liczba_zadan;
        vector <Task> zadania;
        vector <unsigned int> cmin_uszeregowan; //c min dla kazdego uszeregowania
        vector <unsigned int> p_uszeregowan; //suma p dla kazdego uszeregowania
        vector <size_t> wykorzystano_uszeregowanie; //
        vector <size_t> zadanie_na_koncu;

        ifstream plik(nazwa_pliku,ios::out);
        plik >> liczba_zadan;
        for(size_t i=0;i<liczba_zadan;i++)
        {
            Task zadanie;
            plik >> zadanie.p;
            plik >> zadanie.wi;
            plik >> zadanie.ti;
            zadania.push_back(zadanie);
        }

        size_t liczba_krokow=pow(2,liczba_zadan)-1;

        clock_t t = clock();

        //inicjalizacja tablic
        p_uszeregowan.resize(liczba_krokow+1,0);
        cmin_uszeregowan.resize(liczba_krokow+1);
        wykorzystano_uszeregowanie.resize(liczba_krokow+1);
        zadanie_na_koncu.resize(liczba_krokow+1);
        cmin_uszeregowan[0]=0;
        p_uszeregowan[0]=0;


        //algorytm
        for(size_t i=1; i<=liczba_krokow;i++)
        {
            size_t kopia_i = i;
            int cmin = INT_MAX;
            size_t wartosc_najmlodszego=1;
            size_t id_zadania_najmlodszego=0;
            while(kopia_i>0)
            {
                //if((kopia_i % 2) == 1) //czy najmlodszy bit = 1, liczenie c uszeregowania z odpowiadajacym mu zadaniem na koncu
                if((kopia_i & 1) == 1)  //czy najmlodszy bit = 1, liczenie c uszeregowania z odpowiadajacym mu zadaniem na koncu
                {
                    if (p_uszeregowan[i]==0) p_uszeregowan[i] = p_uszeregowan[i-wartosc_najmlodszego]+zadania[id_zadania_najmlodszego].p; //p dla aktualnego uszeregowania
                    int nowe_c = (p_uszeregowan[i] - zadania[id_zadania_najmlodszego].ti)*zadania[id_zadania_najmlodszego].wi; //funkcja celu dla aktualnego uszeregowania
                    if(nowe_c<0) nowe_c=0;
                    nowe_c += cmin_uszeregowan[i-wartosc_najmlodszego];
                    if(nowe_c < cmin)
                    {
                        cmin=nowe_c;
                        wykorzystano_uszeregowanie[i]=i-wartosc_najmlodszego; //ktore poprzednie uszeregowanie wykorzystano w biezacym (do ustalenia optymalnej kolejnosci)
                        zadanie_na_koncu[i]=id_zadania_najmlodszego; //ktore zadanie jest na koncu biezacego uszeregowanie (do ustalenia optymalnej kolejnosci)
                    }
                }
                id_zadania_najmlodszego++;
                wartosc_najmlodszego*=2;
                kopia_i >>= 1;
            }
            cmin_uszeregowan[i]=cmin;
        }

        t = clock() - t;


        //odtwarzanie kolejnosci
        vector<size_t> kolejnosc;
        kolejnosc.resize(liczba_zadan);
        size_t nastepne=liczba_krokow;
        for(auto itr=kolejnosc.rbegin(); itr!=kolejnosc.rend(); itr++)
        {
            (*itr)=zadanie_na_koncu[nastepne]+1;
            if (nastepne!=0)
                nastepne=wykorzystano_uszeregowanie[nastepne];
        }



        //wyswietlanie
        cout << endl << nazwa_pliku << endl;
        cout << "czas: " << (double)t/CLOCKS_PER_SEC << endl;
        cout << cmin_uszeregowan[liczba_krokow] << endl;
        for(auto & itr: kolejnosc)
            cout << itr << " ";
        cout << endl;
    }

    return 0;
}

