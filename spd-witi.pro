TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

QMAKE_CXXFLAGS += -std=c++11

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    task.h

DISTFILES += \
    data10.txt \
    data11.txt \
    data12.txt \
    data13.txt \
    data14.txt \
    data15.txt \
    data16.txt \
    data17.txt \
    data18.txt \
    data19.txt \
    data20.txt

